<?php 

    class animal {
        public $name;
        public $legs = 4;
        public $cold_blooded = 'no';

        public function __construct($name) {
            $this->name = $name;
        }

        public function set_legs($legs) {
            $this->legs = $legs;
        }

        public function set_cold_blooded($cold_blooded) {
            $this->cold_blooded = $cold_blooded;
        }
    }


?>