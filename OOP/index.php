<?php 

    require 'animal.php';
    require 'frog.php';
    require 'ape.php';

    $sheep = new animal('shaun');

    echo "Name:  $sheep->name  <br>"; // "shaun"
    echo "Legs:  $sheep->legs  <br>"; // 4
    echo "Cold Blooded:   $sheep->cold_blooded <br> <br>"; // "no"

    $kodok = new frog("buduk");
    echo "Name:  $kodok->name  <br>";
    echo "Legs:  $kodok->legs  <br>";
    echo "Cold Blooded:   $kodok->cold_blooded  <br>";
    $kodok->jump(); // "hop hop"

    $sungokong = new ape("kera sakti");
    echo "Name:  $sungokong->name  <br>";
    echo "Legs:  $sungokong->legs  <br>";
    echo "Cold Blooded:   $sungokong->cold_blooded  <br>";
    $sungokong->yell(); // "Auooo"

    
?>